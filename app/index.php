<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">

        <title>Задание 1</title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">

    </head>
    <body>
        <main>
            <form id="form" class="decor">
                <div class="form-left-decoration"></div>
                <div class="form-right-decoration"></div>
                <div class="circle"></div>
                <div class="form-inner">
                    <h3>Валидация полей</h3>
                    <div>
                        <input type="text" name="email" placeholder="Email">
                        <p class="error"></p>
                    </div>
                    <div>
                        <input type="phone" name="phone" placeholder="375 29 596 02 95">
                        <p class="error"></p>
                    </div>
                    <div>
                        <input type="text" name="login" placeholder="Login">
                        <p class="error"></p>
                    </div>
                    <div>
                        <input type="number" name="number" placeholder="Just number">
                        <p class="error"></p>
                    </div>
                    <div>
                        <input type="number" name="integer" placeholder="Just integer">
                        <p class="error"></p>
                    </div>
                    <div>
                        <input type="text" name="eng" placeholder="Just Eng Letters">
                        <p class="error"></p>
                    </div>
                    <div>
                        <input type="text" name="letters" placeholder="Just Letters">
                        <p class="error"></p>
                    </div>
                    <div>
                        <input type="text" name="upLetters" placeholder="Just Upp Letters">
                        <p class="error"></p>
                    </div>
                    <div>
                        <input type="number" name="onlyNumbers" placeholder="Only numbers">
                        <p class="error"></p>
                    </div>
                    <div>
                        <input type="text" name="ltrsNumbrs" placeholder="Just letters and numbers">
                        <p class="error"></p>
                    </div>
                    <div>
                        <input type="password" name="password" placeholder="Password">
                        <p class="error"></p>
                    </div>

                    <textarea placeholder="Сообщение..." rows="3"></textarea>
                    <input type="submit" value="Отправить">
                </div>
            </form>
        </main>

        <link rel="stylesheet" href="/main.css">
        <script src="/jquery.min.js"></script>
        <script src="/common.js"></script>
    </body>
</html>