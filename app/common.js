
function checkEmail(email) {
    return /.+@.+\..+/i.test(email);
};

function isPhoneNumber(phoneNumber) {
  return   /^\d{7,12}\d$/.test(phoneNumber);
};

function isNumber(number) {
    return /^-?\d+\.?\d*$/.test(number);
};

function isInteger(integer) {
    return /^([+-]?[1-9]\d*|0)$/.test(integer);
};

function hasOnlyEngLetters(engLetters) {
    return /^[a-zA-Z\s]{2,}$/.test(engLetters)
};

function hasOnlyLetters(letters) {
    return /^[a-zа-яё]+$/i.test(letters)
};

function hasUpperCaseLetters(uppLetters) {
    return /^[A-ZА-ЯЁ]{2,}$/.test(uppLetters)
};

function hasOnlyNumbers(onlyNumbers) {
    return /^\d+$/.test(onlyNumbers)
};

function justLettersAndNumbers(text) {
    return /^[a-zа-яё0-9]+$/i.test(text)
};

function correctPassword(password) {
    return isNumber(password) && hasOnlyEngLetters(password) && hasUpperCaseLetters(password)
};

function isCorrectLogin(login) {
    return checkEmail(login) || isPhoneNumber(login)
};

function bindValidator(formId, validatorsArray) {

    let form = document.getElementById(formId);

    form.addEventListener("submit", function (event) {
        event.preventDefault();

        for (let keys of Object.keys(validatorsArray)) {

            let formField = document.getElementsByName(keys);
            let nodeArray = [].slice.call(formField);
            let fieldValue = nodeArray[0].value;

            if (typeof validatorsArray[keys] === 'object') {

                validatorsArray[keys].forEach(function (item) {
                    let validateMethod = item(fieldValue);
                })

            } else {
                let validateMethod = validatorsArray[keys](fieldValue);
            }

            // if (validateMethod) {
            //     this.submit();
            // } else {
            //     console.log('error')
            // }
        }

    });
}

let validatorsArray = {
    'email' : checkEmail,
    'phone' : isPhoneNumber,
    'password' : [ hasUpperCaseLetters, justLettersAndNumbers ]
};

// console.log(Array.isArray(validatorsArray));

bindValidator('form', validatorsArray);

